# gitlab-runner

[GitLab Runner](https://docs.gitlab.com/runner/) performs scripted CI/CD jobs on behalf of your GitLab group, project, or self-hosted instance.
