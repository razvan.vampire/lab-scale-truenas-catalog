# GitLab Agent app for TrueNAS SCALE

Link your TrueNAS SCALE appliance to a GitLab server for GitOps deployment.

Based on the official Helm chart for the [GitLab agent for
Kubernetes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/)
